#include<stdio.h>

typedef struct Node {
  int data;
  struct Node *next;
} Node;


void printNode(Node *node){
  if(node==NULL) return;
  printf("%d\n", node->data);
  printNode(node->next);
}

void addNode(Node *node, Node *newNode){
  if(node->next==NULL){
    node->next = newNode;
    newNode->next=NULL;
    return;
  }
  addNode(node->next, newNode);
}





int main(void){
  Node n;
  n.data = 15;
  n.next=NULL;

  printNode(&n);
  printf("-------\n");

  Node n2;
  n2.data = 30;

  addNode(&n, &n2);
  printNode(&n);
  printf("-------\n");

  Node n3;
  n3.data = 60;
  
  addNode(&n, &n3);
  printNode(&n);


  return 0;
}
