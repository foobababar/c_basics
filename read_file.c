/* read file line by line demo */

#include<stdio.h>
#include<stdio.h>


int main(void){
  FILE *fp = fopen("lipsum.txt", "r");

  if(!fp){
    fprintf(stderr, "%s\n", "Couldn't open file");
  }

  char *line = NULL;
  size_t linecap = 0;
  ssize_t linelen;

  while((linelen = getline(&line, &linecap, fp)) != -1){
    printf("%s\n", line);
  }
	
  
  return 0;
}
