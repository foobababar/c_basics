/*
 * stack virtual machine
 */
#include<stdio.h>
#include<stdlib.h>

typedef struct Stack {
  int *buf;  
  int len;
} Stack;

//initialize buffer and add 1 zero to it
void init(Stack *stack){
  stack->buf = malloc(1*sizeof(int));
  stack->buf[0]=0;
  stack->len = 1;
}



//print content of stack
void dump(Stack *stack){
  if(stack->len==0) { 
    //don't crash. Just print ...
    printf("stack is empty...\n");
    return;
  }

  for(int i=0;i<stack->len;i++){
    printf("%d ", stack->buf[i]);
  }
  printf("\n");
}


// add item on top of stack
void add(Stack *stack, int nb){
  int *tmp = malloc((stack->len+1)*sizeof(int));
  for(int i=0;i<stack->len;i++){
    tmp[i]=stack->buf[i];
  }
  free(stack->buf);

  tmp[stack->len]=nb;
  stack->buf = tmp;
  stack->len++;
}

//remove item from top of stack
int pop(Stack *stack){
  if(stack->len==0) {
    fprintf(stderr, "%s\n", "attempted to pop from empty stack");
    exit(1);
  }

  int *tmp = malloc((stack->len-1) * sizeof(int));
  for(int i=1; i<stack->len; i++){
    tmp[i-1]=stack->buf[i];
  }
  free(stack->buf);
  stack->buf = tmp;
  stack->len--;
  return 0;
}




int main(void){
  Stack stack;
  init(&stack);

  add(&stack, 25);
  add(&stack, 50);
  dump(&stack);

  pop(&stack);
  pop(&stack);
  pop(&stack);
  dump(&stack); //stack is empty ...

  add(&stack, 89);
  add(&stack, 154);
  dump(&stack);

  pop(&stack);
  pop(&stack); //stack is empty


  add(&stack, 77);
  dump(&stack);


  free(stack.buf);

  return 0;
}




