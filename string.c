/*
 * simple string datatype demo
 */

#include<stdio.h>
#include<stdlib.h>
#include<string.h>

typedef struct String {
  char *buf;
  int len;
} String;

void initString(String *self){
  self->buf = NULL;
  self->len = 0;
}

void add2String(String *self, char* input){
  if(self->len==0){ //not initialized yet
    self->buf = malloc(strlen(input)+1);
    strcpy(self->buf, input);
    self->len = strlen(input);
  }
  else {
    char *tmp = malloc(self->len+1);
    strcpy(tmp, self->buf);

    self->buf = realloc(self->buf, self->len+strlen(input)+1);

    strcat(self->buf, input);

    self->len = strlen(self->buf);
    free(tmp);
  }
}

void stringFree(String *self){
  free(self->buf);
}


int main(void) {
  String lst;	
  //String lst = {NULL, 0};  //initialize like so ?
  initString(&lst);

  add2String(&lst, "bar");
  printf("%s\n", lst.buf);
  printf("%d\n", lst.len);

  add2String(&lst, "foo");

  printf("%s\n", lst.buf);
  printf("%d\n", lst.len);

  add2String(&lst, "salem");

  printf("%s\n", lst.buf);
  printf("%d\n", lst.len);

  stringFree(&lst);
  return 0;
}


