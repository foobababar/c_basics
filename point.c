/*
 * simple C struct demo
 */

#include<stdio.h>

typedef struct Point {
  int x;
  int y;
} Point;


int main(void){
  Point p;	
  p.x = 4;
  p.y = 5;
  
  printf("%d %d \n", p.x, p.y);
  	
  return 0;
}
