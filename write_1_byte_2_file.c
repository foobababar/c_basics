/*
 * Write a single byte to a file.
 * Overwrite if already exists.
 * Creates if doesn't exist.
 */


#include<stdio.h>
#include<stdlib.h>

int main(void) {
  FILE *f;
  f = fopen("foo.txt", "wb");

  if(f == NULL){
    fprintf(stderr, "%s", "Error opening file...");  
    exit(1);
  }

  char byte = 255;
  fwrite(&byte, sizeof(char) ,1 , f);


  fclose(f);
  return 0;
}

