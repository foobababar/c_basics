/*
 * allocate memory for struct demo
 */

#include<stdio.h>
#include<stdlib.h>

typedef struct Point {
  int x;
  int y;
} Point;


Point* createPoint(int x, int y){
  Point *p = malloc(sizeof(Point));
  
  if(p == NULL){ //malloc can fail, but unlikely
    fprintf(stdout, "%s", "Error allocating memory...");
    exit(1);
  }

  p->x = x;
  p->y = y;
  return p;
}



int main(void){
  Point *p = createPoint(75, 12); 
  printf("%d %d\n", p->x, p->y);
  free(p);

  return 0;
}
