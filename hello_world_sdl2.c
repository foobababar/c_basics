// SDL2 Hello, World!
// compile with : gcc main.c -lSDL2 -o out
// run with ./out

#include <SDL2/SDL.h>
#include <stdio.h>
#include <stdbool.h>

#define SCREEN_WIDTH 640
#define SCREEN_HEIGHT 480

int main(int argc, char* args[]) {
    SDL_Window* window = NULL;
    SDL_Surface* screenSurface = NULL;
    SDL_Renderer* renderer = NULL;
    SDL_Texture* bg = NULL;

    if (SDL_Init(SDL_INIT_VIDEO) < 0) {
        fprintf(stderr, "could not initialize sdl2: %s\n", SDL_GetError());
        return 1;
    }

    window = SDL_CreateWindow("hello_sdl2", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, SCREEN_WIDTH, SCREEN_HEIGHT, SDL_WINDOW_SHOWN);
    renderer = SDL_CreateRenderer(window, -1, SDL_RENDERER_ACCELERATED);
    SDL_SetRenderDrawBlendMode(renderer, SDL_BLENDMODE_BLEND);

    bg = SDL_CreateTexture(renderer, SDL_PIXELFORMAT_ARGB8888, SDL_TEXTUREACCESS_STREAMING, SCREEN_WIDTH, SCREEN_HEIGHT);
    SDL_SetTextureBlendMode(bg, SDL_BLENDMODE_BLEND);

    if (window == NULL) {
        fprintf(stderr, "could not create window: %s\n", SDL_GetError());
        return 1;
    }

    SDL_Rect rect;
    rect.x = 0;
    rect.y = 0;
    rect.w = 100;
    rect.h = 100;


    uint32_t canvas[SCREEN_WIDTH*SCREEN_HEIGHT];
    for (int i=0; i<SCREEN_WIDTH*SCREEN_HEIGHT; i++) {
        if(i<SCREEN_WIDTH*SCREEN_HEIGHT/2){
            canvas[i] = 0xFFFF4444;
        }
        else {
            canvas[i] = 0xFFFFFFFF;
        }
    }

    uint32_t frame_start = 0;
    uint32_t frame_end= 0;
    uint32_t frame_count= 0;
    bool quit = false;
    while (!quit) {
        frame_count = SDL_GetTicks();
        frame_start = frame_count;

        SDL_Event event;
        while (SDL_PollEvent(&event) != 0) {
            switch (event.type) {
                case SDL_QUIT:
                    quit = true;
                    break;

                case SDL_KEYDOWN:
                    switch (event.key.keysym.sym) {
                        case SDLK_ESCAPE:
                            quit = true;
                            break;

                        case SDLK_SPACE :
                            break;

                        case SDLK_q :
                            quit = true;
                            break;

                        case SDLK_h:
                            break;
                        case SDLK_l:break;

                        case SDLK_LEFT: 
                            rect.x-=10;
                            break;

                        case SDLK_RIGHT: 
                            rect.x+=10;
                            break;

                        case SDLK_UP: 
                            rect.y-=10;
                            break;

                        case SDLK_DOWN: 
                            rect.y+=10;
                            break;
                        default : 
                    }
                    break;

                case SDL_KEYUP:
                    switch (event.key.keysym.sym) {
                        case SDLK_ESCAPE:
                            quit = true;
                            break;
                        case SDLK_SPACE :
                            break;

                        case SDLK_q :
                            quit = true;
                            break;
                        case SDLK_h:break;

                        case SDLK_l:break;

                        case SDLK_LEFT: break;

                        case SDLK_RIGHT: break;

                        case SDLK_UP: break;

                        case SDLK_DOWN: break;
                        default : 
                    }
                    break;

                case SDL_MOUSEWHEEL:
                    if (event.wheel.y > 0) {} else if (event.wheel.y < 0) {}
                    break;
                case SDL_MOUSEBUTTONDOWN:
                    if (event.button.button == SDL_BUTTON_LEFT) {} else if (event.button.button == SDL_BUTTON_RIGHT) {}
                    break;
                case SDL_MOUSEBUTTONUP:
                    if (event.button.button == SDL_BUTTON_LEFT) {} else if (event.button.button == SDL_BUTTON_RIGHT) {}
                    break;
                case SDL_MOUSEMOTION:
                    break;
                default:
            }
        }
        SDL_UpdateTexture(bg, NULL, &canvas, SCREEN_WIDTH * sizeof(uint32_t));
        SDL_RenderClear(renderer);
        SDL_RenderCopy(renderer, bg, NULL, NULL);
        // draw rectangle
        SDL_SetRenderDrawColor(renderer, 0, 0, 0, 255); // < set drawing color to white
        SDL_RenderFillRect(renderer, &rect);

        // set drawing color to white
        SDL_SetRenderDrawColor(renderer, 255, 255, 255, 255);

        // render everything ?
        SDL_RenderPresent(renderer);

        // ~~~~ regulate fps ~~~~
        frame_end = SDL_GetTicks();

        const int elapsed_time = frame_end - frame_start;
        const int time_wanted = 1000 / 60;

        if (elapsed_time < time_wanted) {
            SDL_Delay(time_wanted - elapsed_time);
        }
    }

    SDL_DestroyTexture(bg);
    SDL_DestroyRenderer(renderer);
    SDL_DestroyWindow(window);
    SDL_Quit();
    return 0;
}