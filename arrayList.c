/* read file line by line demo */

#include<stdio.h>
#include<stdio.h>
#include<string.h>
#include<stdlib.h>


typedef struct ArrayList {
  char **buf;
  int len;
}ArrayList;


void initArrayList(ArrayList *self){
  self->buf = NULL;
  self->len = 0;
}

void add2ArrayList(ArrayList *self, char *new){
  if(self->len==0){
    self->buf = malloc(1*sizeof(char*));
    self->len++;

    self->buf[0] = new;
  }
  else {
    self->len++;
    self->buf = realloc(self->buf, self->len*sizeof(char*));
    self->buf[self->len-1]=new;
  }
}

void printArrayList(ArrayList *self){
  for(int i=0;i<self->len;i++){
    if(strcmp(self->buf[i], "\n") != 0)printf("%s\n", self->buf[i]);
  }
}


void freeArrayList(ArrayList *self){
  for(int i=0;i<self->len;i++){
    free(self->buf[i]);
  }
  free(self->buf);
}

int main(void){
  FILE *fp = fopen("lipsum.txt", "r");

  if(!fp){
    fprintf(stderr, "%s\n", "Couldn't open file");
  }

  char *line = NULL;
  size_t linecap = 0;
  ssize_t linelen;

  ArrayList lst;
  initArrayList(&lst);


  while((linelen = getline(&line, &linecap, fp)) != -1){
    char *ptr = malloc((linelen+1)*sizeof(char));
    strcpy(ptr, line);
    add2ArrayList(&lst, ptr);
  }
  free(line);


  printArrayList(&lst);
  freeArrayList(&lst);

  return 0;
}
