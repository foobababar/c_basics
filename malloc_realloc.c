#include<stdio.h>
#include<stdlib.h>
#include<string.h>


int main(void){
  char* str = malloc(4); //allocate 1 more byte for '\0'
  strcpy(str, "bar");
  printf("%s\n", str); //str is "bar\0"

  str = realloc(str, 7); //str is "bar\0\0\0\0"

  strcat(str, "foo"); //str is "barfoo\0"

  printf("%s\n", str);

  free(str);

  return 0;
}
